import tensorflow as tf


SEGMENT_CLASSES = {
    0: 'NOT tumor',
    1: 'NECROTIC/CORE',
    2: 'EDEMA',
    3: 'ENHANCING'
}


NUM_CLASSES = 4


VOLUME_SLICES = 100
VOLUME_START_AT = 22


TRAIN_DATASET_PATH = '/storage/sorinaau/data/dataset/training_data/'
VALIDATION_DATASET_PATH = '/storage/sorinaau/data/dataset/validation_data'


RESNET50_WEIGHTS_PATH = "/storage/sorinaau/disertation_thesis/weights/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5"


IMG_SIZE = 128


def lr_scheduler(epoch, lr):
    # Cosine annealing
    return lr * tf.math.cos(tf.constant(epoch) * (3.14 / 20))

# Define SGDR learning rate schedule
# def cosine_annealing_with_warm_restart(epoch, lr):
#     initial_lr = 1e-4
#     min_lr = 1e-7
#     T = 10 # Number of epochs between restarts
#     M = 0.5 # Multiplicative factor by which the learning rate is decayed
#     cycles = np.floor(epoch / T)
#     restart_lr = initial_lr * (M ** cycles)
#     return max(restart_lr, min_lr)
