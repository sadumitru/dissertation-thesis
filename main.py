# RUN THIS BEFORE OR ADD IN CONDA ENV
#  export XLA_FLAGS=--xla_gpu_cuda_data_dir=/storage/sorinaau/miniconda3/pkgs/cuda-nvcc-12.4.131-0

from keras.metrics import MeanIoU
from keras.optimizers import Adam, SGD
from utils.utils import *
from metrics.validation_metrics import *
from models.unet import build_unet
from models.deeplabv3 import build_deeplabv3
from models.segnet import build_segnet
from keras.layers import Input
from keras.regularizers import l2
from keras.utils import plot_model
from data_loader.data_generator import DataGenerator, get_data_indices, show_data_layout
import os
from keras.callbacks import CSVLogger, ReduceLROnPlateau, EarlyStopping, ModelCheckpoint, LearningRateScheduler


input_layer = Input((IMG_SIZE, IMG_SIZE, 2))
loss_cat_crossentropy = "categorical_crossentropy"
loss_bin_crossentropy = 'binary_crossentropy'
ker_init = "he_normal"
dropout = 0.2
kernel_regularizer = l2(1e-5)
adam_learning_rate = 0.001
adam_optimizer = Adam(learning_rate=adam_learning_rate)
metrics = ['accuracy', MeanIoU(
    num_classes=4), dice_coef, precision, sensitivity, specificity, dice_coef_necrotic, dice_coef_edema, dice_coef_enhancing]


# splt training data into training, testing and validation
train_ids, test_ids, val_ids = get_data_indices()

# print training data distribution
show_data_layout(train_ids, test_ids, val_ids)

# create data generators
training_generator = DataGenerator(train_ids)
test_generator = DataGenerator(test_ids)
validation_generator = DataGenerator(val_ids)

# create callbacks
csv_logger = CSVLogger('training.log', separator=',', append=False)
callbacks = [
    #     EarlyStopping(monitor='loss', min_delta=0,
    #                               patience=2, verbose=1, mode='auto'),
    ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                      patience=2, min_lr=0.000001, verbose=1),
    #  ModelCheckpoint(filepath = 'model_.{epoch:02d}-{val_loss:.6f}.m5',
    #                             verbose=1, save_best_only=True, save_weights_only = True)
    csv_logger
]

# ### U-Net
# # build and plot model
unet_model = build_unet(input_layer, ker_init, dropout)
unet_model.compile(loss=loss_cat_crossentropy,
                   optimizer=adam_optimizer, metrics=metrics)
plot_model(unet_model,
           show_shapes=True,
           show_dtype=False,
           show_layer_names=True,
           rankdir='TB',
           expand_nested=False,
           to_file="/storage/sorinaau/disertation_thesis/images/unet_architecture.png",
           dpi=70)

# # trian model
K.clear_session()
unet_history = unet_model.fit(training_generator,
                              epochs=35,
                              steps_per_epoch=len(train_ids),
                              callbacks=callbacks,
                              validation_data=validation_generator
                              )
unet_model.save("unet_model.h5")

# ### DeepLabV3+
# # build and plot model
deeplabv3_model = build_deeplabv3(input_layer, ker_init)
deeplabv3_model.compile(loss=loss_cat_crossentropy,
                        optimizer=adam_optimizer, metrics=metrics)
plot_model(deeplabv3_model,
           show_shapes=True,
           show_dtype=False,
           show_layer_names=True,
           rankdir='TB',
           expand_nested=False,
           to_file="/storage/sorinaau/disertation_thesis/images/deeplabv3_architecture.png",
           dpi=70)

# # train model
# K.clear_session()
deeplabv3_history = deeplabv3_model.fit(training_generator,
                                        epochs=35,
                                        steps_per_epoch=len(train_ids),
                                        callbacks=callbacks,
                                        validation_data=validation_generator
                                        )
deeplabv3_model.save("deeplabv3_model.h5")


# segnet
# build and plot model
segnet_model = build_segnet(input_layer, ker_init)
segnet_model.compile(loss=loss_cat_crossentropy,
                     optimizer=adam_optimizer, metrics=metrics)
plot_model(segnet_model,
           show_shapes=True,
           show_dtype=False,
           show_layer_names=True,
           rankdir='TB',
           expand_nested=False,
           to_file="/storage/sorinaau/disertation_thesis/images/segnet_architecture.png",
           dpi=70)

# train model
K.clear_session()
segnet_history = segnet_model.fit(training_generator,
                                  epochs=35,
                                  steps_per_epoch=len(train_ids),
                                  callbacks=callbacks,
                                  validation_data=validation_generator
                                  )
segnet_model.save("segnet_model.h5")


# Two U-Nets trained on different pipelines whose weights are averaged
# U-Net 1 training: batch size 8, Adam optimizer (lr=1e-4), validation performed each 3 epochs with early stopping, no weight decay.
unet_model_1 = build_unet(input_layer, ker_init, dropout)
unet_model_1.compile(optimizer=Adam(lr=1e-4),
                     loss=loss_cat_crossentropy, metrics=metrics)
callbacks_unet_model_1 = [
    EarlyStopping(monitor='val_loss', patience=3,
                  verbose=1, restore_best_weights=True)
]

unet_model_1_history = unet_model_1.fit(training_generator,
                                        epochs=100,
                                        batch_size=8,
                                        validation_data=validation_generator,
                                        callbacks=callbacks)
unet_model_1.save("unet_model_1.h5")

# U-Net 2 training: batch size 16, Stochastic Gradient Descent With Restarts (lr=1e-4 and cosine annealing), L2 regularisation.
unet_model_2 = build_unet(input_layer, ker_init, dropout, kernel_regularizer)
unet_model_2.compile(optimizer=SGD(lr=1e-4),
                     loss=loss_bin_crossentropy, metrics=metrics)
callbacks_unet_model_2 = [
    EarlyStopping(monitor='val_loss', patience=3,
                  verbose=1, restore_best_weights=True),
    LearningRateScheduler(lr_scheduler)
]

unet_model_2_history = unet_model_2.fit(training_generator,
                                        epochs=100,
                                        batch_size=16,
                                        validation_data=validation_generator,
                                        callbacks=callbacks)
unet_model_2.save("unet_model_2.h5")

# weights averaging
for layer1, layer2 in zip(unet_model_1.layers, unet_model_2.layers):
    if layer1.get_weights() and layer2.get_weights():
        layer1_weights = layer1.get_weights()
        layer2_weights = layer2.get_weights()
        averaged_weights = [(w1 + w2) / 2 for w1,
                            w2 in zip(layer1_weights, layer2_weights)]
        layer1.set_weights(averaged_weights)
unet_model_1.save("unet_model_averaged_weights.h5")
