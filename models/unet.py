from keras.models import Model
from keras.layers import Conv2D, MaxPooling2D, Dropout, Concatenate, UpSampling2D
from utils.utils import *


def build_unet(inputs, ker_init, dropout, kernel_regularizer=None):
    conv1 = Conv2D(32, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(inputs)
    conv1 = Conv2D(32, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv1)

    pool = MaxPooling2D(pool_size=(2, 2))(conv1)
    conv = Conv2D(64, 3, activation='relu', padding='same',
                  kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(pool)
    conv = Conv2D(64, 3, activation='relu', padding='same',
                  kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv)

    pool1 = MaxPooling2D(pool_size=(2, 2))(conv)
    conv2 = Conv2D(128, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(pool1)
    conv2 = Conv2D(128, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv2)

    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3 = Conv2D(256, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(pool2)
    conv3 = Conv2D(256, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv3)

    pool4 = MaxPooling2D(pool_size=(2, 2))(conv3)
    conv5 = Conv2D(512, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(pool4)
    conv5 = Conv2D(512, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv5)
    drop5 = Dropout(dropout)(conv5)

    up7 = Conv2D(256, 2, activation='relu', padding='same',
                 kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(UpSampling2D(size=(2, 2))(drop5))
    merge7 = Concatenate([conv3, up7], axis=3)
    conv7 = Conv2D(256, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(merge7)
    conv7 = Conv2D(256, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv7)

    up8 = Conv2D(128, 2, activation='relu', padding='same',
                 kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(UpSampling2D(size=(2, 2))(conv7))
    merge8 = Concatenate([conv2, up8], axis=3)
    conv8 = Conv2D(128, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(merge8)
    conv8 = Conv2D(128, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv8)

    up9 = Conv2D(64, 2, activation='relu', padding='same',
                 kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(UpSampling2D(size=(2, 2))(conv8))
    merge9 = Concatenate([conv, up9], axis=3)
    conv9 = Conv2D(64, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(merge9)
    conv9 = Conv2D(64, 3, activation='relu', padding='same',
                   kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv9)

    up = Conv2D(32, 2, activation='relu', padding='same',
                kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(UpSampling2D(size=(2, 2))(conv9))
    merge = Concatenate([conv1, up], axis=3)
    conv = Conv2D(32, 3, activation='relu', padding='same',
                  kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(merge)
    conv = Conv2D(32, 3, activation='relu', padding='same',
                  kernel_initializer=ker_init, kernel_regularizer=kernel_regularizer)(conv)

    conv10 = Conv2D(4, (1, 1), activation='softmax')(conv)

    return Model(inputs=inputs, outputs=conv10)
